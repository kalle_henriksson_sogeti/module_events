<?php
	class ImageSize{
		public $width;
		public $height;

		public function __construct($image){
			$this->width = imagesx($image);
			$this->height = imagesy($image);
		}
	}
?>