<?php
	require_once 'class.ImageSize.php';

	class EventImage{
		public $base64 = "";
		public $image_size = null;

		public function __construct($base64 = null) {
			if($base64 != null && $base64 != ""){
				$this->initData($base64);
			}
		}

		public function createFromImageResource($imageResource = null, $mime_type = null){
			if($imageResource == null || $mime_type == null || $mime_type == ""){
				return;
			}

			ob_start();
			switch ($mime_type) {
		    	case 'image/jpeg':
		    	imagejpeg($imageResource);
		    		break;
		    	case 'image/gif':
		    	imagegif($imageResource);
		    		break;
		    	case 'image/png':
		    	imagepng($imageResource);
		    		break;
		    	
		    	default:
		    		break;
		    }
			imagepng($imageResource);
			$imgObj = ob_get_clean();

			$this->initData(base64_encode($imgObj));
		}

		private function initData($base64){
			$this->base64 = $base64;
			$this->image_size = new ImageSize($this->getImage());
		}

		public function getMimeType(){
			$raw = $this->_getImageRaw();
			if($raw == null){
				return null;
			}

			$f = finfo_open();
			return finfo_buffer($f, $raw, FILEINFO_MIME_TYPE);
		}

		public function getBase64(){
			return $this->base64;
		}

		public function getImage(){
			$raw = $this->_getImageRaw();
			if($raw == null){
				return null;
			}
			return imagecreatefromstring($raw);
		}

		public function getSize(){
			return $this->image_size;
		}

		private function _getImageRaw(){
			if($this->base64 == null){
				return null;
			}
			return base64_decode($this->base64);
		}
	}
?>