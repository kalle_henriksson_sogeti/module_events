<?PHP  

	if (!defined('BASEPATH')) exit('No direct script access allowed');

	require_once 'helper_classes/class.EventImage.php';

	
	
	/**
	@Module:		Modules
	@Name:			modules_helper.php
	---------------------------------------------------------------------------------------------------------------
	@Creator:		Luckylane, Johan Lindqvist
	@Created:		2014
	@Version:		1.0
	@PHP Version: 	5	
	---------------------------------------------------------------------------------------------------------------
	@Description	Denna fil inneh�ller gemensamma funktioner f�r hanteringen av moduler p� webbplatsen.
	
	@History
	DATE			AUTHOR				ACTION
	2014-10-14		Johan Lindqvist		Skapade modulen.
		
	*/




	/**
	@Name:			_events_get_events_data($language, $published, $status=null, $account_id=null)
	---------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Param			
	
	@Return						
	---------------------------------------------------------------------------------------------------------------
	@Description: 	Funktionen h�mtar in alla events baserat p� spr�k, publicerad-flagga, status-flagga och konto-id.
	
	@History
	DATE			AUTHOR				ACTION
	2015-02-23		Kalle Henriksson	Skapade funktionen.
		
	*/
    function _events_get_events_data($language, $published=null, $status=null, $account_id=null)
	{
		$ci = & get_instance();
		
		//	Filtrera p� angivet spr�k
		$ci->db->join('events_content','events_content.ec_event_id = events.event_id');
		$ci->db->where('events_content.ec_language', $language);
		
		//	Filtrera p� publicerad
		if($published)
		{			
			$ci->db->where('events.event_published', $published);
		}
		
		//	Filtrera p� konto-id
		if($account_id != null)
		{			
			$ci->db->where('events.event_account_id', $account_id);
		}
		
		//	Filtrera p� status
		if($status != null){
			$ci->db->where('events.event_status', $status);
		}else{
			$ci->db->where('events.event_status > ', '0');
		}

		$ci->db->order_by('events.event_start_date', "desc"); 
		$ci->db->order_by('events.event_end_date', "desc"); 
		
		$query	=	$ci->db->get('events');


		return $query;

	}





















	/**
	@Name:			_events_get_event_data($event_id)
	---------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Param			$event_id, $language
	
	@Return						
	---------------------------------------------------------------------------------------------------------------
	@Description: 	H�mtar ett visst event.
	
	@History
	DATE			AUTHOR				ACTION
	2015-02-23		Kalle Henriksson	Skapade funktionen.
		
	*/
	function _events_get_event_data($event_id, $language){
		$ci = & get_instance();
		$ci->db->where('events.event_id', $event_id);
		$ci->db->join('events_content','events_content.ec_event_id = events.event_id');
		$ci->db->where('events_content.ec_language', $language);


		$query = $ci->db->get('events');

		return $query;
	}



	/**
	@Name:			_events_get_events_companies_accounts_data($account_id)
	---------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Param			$account_id
	
	@Return						
	---------------------------------------------------------------------------------------------------------------
	@Description: 	H�mtar alla f�retag kopplade till kontot. Dessa f�retag kan senare kopplas till ett eller flera event (som utst�llare).
	
	@History
	DATE			AUTHOR				ACTION
	2015-02-23		Kalle Henriksson	Skapade funktionen.
		
	*/
	function _events_get_events_companies_accounts_data($account_id){
		$ci = & get_instance();
// lcms_events_companies_accounts
// eca_id
// eca_cc_id
// eca_account_id
// eca_created
// eca_created_by
// eca_updated
// eca_updated_by
		if($account_id){
			$ci->db->where('events_companies_accounts.eca_account_id', $account_id);
		}
		
		$ci->db->join('contacts_companies','contacts_companies.company_id = events_companies_accounts.eca_cc_id');

		$ci->db->where('contacts_companies.company_status', 1);

		$query = $ci->db->get('events_companies_accounts');


/*		var_dump($ci->db->last_query());
		exit();*/

		return $query;
	}



	/**
	@Name:			_events_get_events_companies_exhibitors_data($event_id)
	---------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Param			$event_id
	
	@Return						
	---------------------------------------------------------------------------------------------------------------
	@Description: 	H�mtar alla f�retag som kopplats till ett angivet event.
	
	@History
	DATE			AUTHOR				ACTION
	2015-02-23		Kalle Henriksson	Skapade funktionen.
		
	*/
	function _events_get_events_companies_exhibitors_data($event_id){
		$ci = & get_instance();
// lcms_events_companies_exhibitors
// ece_id
// ece_event_id
// ece_eca_id
// ece_created
// ece_created_by
		$ci->db->where('events_companies_exhibitors.ece_event_id', $event_id);
		$ci->db->join('events_companies_accounts','events_companies_accounts.eca_id = events_companies_exhibitors.ece_eca_id');
		$ci->db->join('contacts_companies','contacts_companies.company_id = events_companies_accounts.eca_cc_id');

		$query = $ci->db->get('events_companies_exhibitors');

		return $query;
	}	


	function _events_get_event_image_data($event_id){
		$ci = & get_instance();

		$ci->db->limit(1);
		$ci->db->select("events.event_img");
		$ci->db->where('events.event_id', $event_id);

		$row = $ci->db->get('events')->row();
		
		$base64 = null;

		if($row->event_img != ""){
			$path = $row->event_img;
			$type = pathinfo($path, PATHINFO_EXTENSION);
			$data = file_get_contents($path);
			$base64 = base64_encode($data);
		}

		return $base64;
	}

	function _events_get_image_resized_data($event_id, $new_width = 0, $new_height = 0){
		$image_data = _events_get_event_image_data($event_id);
		if($image_data == null){
			return new EventImage();
		}

		//	Konvertera fr�n Base64
		$bin_data = base64_decode($image_data);
		$f = finfo_open();
		//	Ta reda p� vilken MimeType bilden har (format)
		$mime_type = finfo_buffer($f, $bin_data, FILEINFO_MIME_TYPE);

		//	Skapa bilddata i minnet att arbeta med och ta reda p� dess originalbredd och h�jd.
        $im = imagecreatefromstring($bin_data);
		$orig_w = imagesx($im);
		$orig_h = imagesy($im);

		if($new_width < 1 || $new_height < 1){
			$new_width = $orig_w;
			$new_height = $orig_h;
		}

		//	Beh�ll proportionerna
		$ratio = ($orig_w / $orig_h);

		//	�r originalet liggande?
		if($ratio >= 1) {
			$new_height = $new_width / $ratio;
		} else {
			$new_width = $new_height * $ratio;
		}

		//	Skapa en ny bild med angivna m�tt
		$img = imagecreatetruecolor($new_width, $new_height);
		//	Kopiera bilddatat fr�n k�llbilden till den nya
        imagecopyresized($img, $im, 0, 0, 0, 0, $new_width, $new_height, $orig_w, $orig_h);

		$image = new EventImage();
		$image->createFromImageResource($img, $mime_type);

		return $image;
	}




?>