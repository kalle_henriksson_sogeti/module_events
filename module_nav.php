<li>
    <a href="#" id="<?PHP echo lang('events_module_name'); ?>">
		<i class="fa fa-rocket fw"></i>
		<?PHP echo lang('events_headline'); ?>
    </a>
    <ul class="sub">
        <li><a href="<?PHP echo site_url('admin/events') ?>"><?PHP echo lang('system_dashboard'); ?></a></li>
        <li><a href="<?PHP echo site_url('admin/programs') ?>"><?PHP echo lang('system_events_programs_dashboard'); ?></a></li>
        <li><a href="<?PHP echo site_url('admin/activities') ?>"><?PHP echo lang('system_events_activities_dashboard'); ?></a></li>
	</ul>
</li>