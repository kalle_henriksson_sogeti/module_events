<?PHP

	if (!defined('BASEPATH')) exit('No direct script access allowed');


	/**
	@Module:		Manager
	@Name:			manager.php
	--------------------------------------------------------------------------------------------------
	@Creator:		Luckylane, Johan Lindqvist
	@Created:		2014
	@Version:		1.0
	@PHP Version: 	5	
	--------------------------------------------------------------------------------------------------
	@Description	Denna fil hanterar kontoadministratörens tjänster. Skalskyddet för modulen hanteras av
					luckycms/modules/core/users och anropas via $this->auth->user_logged_in().
	
	@History
	DATUM			VEM						ÅTGÄRD
	2014-05-05		Johan Lindqvist			Skapade filen	
	
	*/
	
class Events extends Public_Controller
{
	
	function __construct()
	{
		
		parent::Public_Controller();
				
		// Laddar in nödvändiga bibliotek och filer.
		
		// Läser in bibliotek för skalskydd.
		$this->load->library(array('auth','form_validation','email'));

		// Läser in models.
//		$this->load->model(array('events/events_model', 'contact/contact_model', 'accounts/accounts_model','sms/sms_model'));
		
		// Läser in helpers.
//		$this->load->helper(array('events/events', 'contact/contact','accounts/accounts','sms/sms'));

		// Läser in models
		$this->load->model(array(
			'language_model',
			'events/events_model',
			'contact/contact_model',
			'accounts/accounts_model',
			'programs/programs_model',
		));
		
		// Läser in helpers
		$this->load->helper(array(
			'language',
			'url',
			'events',
			'programs/programs',
			'contact/contact',
			'form',
			'accounts/accounts'
		));
		
		// Läser in språk.
		$this->lang->load('events', $this->session->userdata('language_name'));
		$this->lang->load('programs/programs', $this->session->userdata('language_name'));
		$this->lang->load('accounts/accounts', $this->session->userdata('language_name'));
		$this->lang->load('sms/sms', $this->session->userdata('language_name'));
		$this->lang->load('contact/contact', $this->session->userdata('language_name'));
		
	}
	
	// Är användaren inloggad visas vyn dashboard annars visas vyn för inloggning.
	function index()
	{
		// Hämtar in publicerade och aktiva poster för aktuellt språk
		$data['events_list']		=	$this->events_model->get_events();
				
		// Sätter variabler för att kunna presentera layouten
		$data['cur_slug']		=	$this->uri->segment(1);		
		$data['default_index'] 	= 	$this->site_options['index_page'];
        $this->template->set_theme($this->site_options['site_theme']);
		
        $this->template->set_layout('layout');
				 
		// Läser in vyn som skall presentera posterna                                
        $this->template->title(lang('events_headline'))->build('public_eventslist', $data);
	}

	function programs(){
		$event_id						=	$this->uri->segment(3);

		// Hämtar in ett event 
		$data['event']					=	$this->events_model->get_event($event_id);

		//	...och eventets program
		$data['event_programs']			=	$this->programs_model->get_programs_for_event('sv', 1, 1, $event_id);

		// Sätter variabler för att kunna presentera layouten
		$data['cur_slug']				=	$this->uri->segment(1);		
		$data['default_index'] 			= 	$this->site_options['index_page'];
        $this->template->set_theme($this->site_options['site_theme']);
		
        $this->template->set_layout('layout');
				 
		// Läser in vyn som skall presentera posterna                                
        $this->template->title(lang('events_headline'))->build('public_event_programs_list', $data);
	}

	function my_program(){

		$event_id						=	$this->uri->segment(3);

		$contact_id						=	$this->session->userdata('user_id');

		$data['event_program']			=	$this->programs_model->get_programs_for_event('sv',1, 1, $event_id, $contact_id);

		if(sizeof($data['event_program']) == 0){
			$this->programs_model->save_program();
		}



		if(sizeof($data['event_program']) > 0){
			$program 						=	$data['event_program'][0];
			$ep_id 							=	$program->ep_id;
			$data['program_activities']		=	$this->programs_model->get_program_activities($ep_id, 'sv', true);	//	program_id, $language='sv', $only_active = false
		} else {
			$data['program_activities'] 	=	array();
		}

				

		// Sätter variabler för att kunna presentera layouten
		$data['cur_slug']		=	$this->uri->segment(1);		
		$data['default_index'] 	= 	$this->site_options['index_page'];
        $this->template->set_theme($this->site_options['site_theme']);
		
        $this->template->set_layout('layout');

		$this->template->title(lang('events_headline'))->build('public_event_program', $data);
	}

	function event(){
		$event_id	=	$this->uri->segment(3);

		// Hämtar in ett event 
		$data['event']			=	$this->events_model->get_event($event_id);

		// Sätter variabler för att kunna presentera layouten
		$data['cur_slug']		=	$this->uri->segment(1);		
		$data['default_index'] 	= 	$this->site_options['index_page'];
        $this->template->set_theme($this->site_options['site_theme']);
		
        $this->template->set_layout('layout');

		$this->template->title(lang('events_headline'))->build('public_event', $data);
	}

	function get_image(){

	}

	function get_image______FOR_THE_FUTURE(){
		$event_id	=	$this->uri->segment(3);
		$new_width	=	$this->uri->segment(4);
		$new_height	=	$this->uri->segment(5);

		$eventImage = $this->events_model->get_event_image_resized($event_id, $new_width, $new_height);

        if($eventImage == null){
        	die("An error ocurred when aquiring image.");
        }

        $mime_type = $eventImage->getMimeType();

        //	Baserat på mime-typ, returnera rätt typ av bild.
	    header('Content-Type: ' . $mime_type);
	    switch ($mime_type) {
	    	case 'image/jpeg':
	    imagejpeg($eventImage->getImage());
	    		break;
	    	case 'image/gif':
	    imagegif($eventImage->getImage());
	    		break;
	    	case 'image/png':
	    imagepng($eventImage->getImage());
	    		break;
	    	
	    	default:
	    		break;
	    }

	    imagedestroy($eventImage);
	}


	//	En ingång för att få ut eventlistan i json-format. Tänkt för appens eventlista.
	function events_json(){
		// Hämtar in publicerade och aktiva poster för aktuellt språk
		$account_id = $this->uri->segment(3);
		$event_datas = $this->events_model->get_events('sv', $account_id);

		// Bygger en bantad lista med enbart det som appen är intrsserad av.
		$current_events = array();
		$past_events = array();
		$d_now = strtotime("now");

		$current_url = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'];
		$parsedUrl = parse_url($current_url);
		$base_url = $parsedUrl['scheme'] . '://' . $parsedUrl['host'] . '/';

		foreach ($event_datas as $e) {
			if($e->event_published != 1 || $e->event_status == 0){
				continue;
			}
			$d_start = strtotime($e->event_start_date . "T00:00:00+01:00");
			$d_end = strtotime($e->event_end_date . "T23:59:59+01:00");

			$event_post = array(
				"id" => $e->event_id,
				"start_date" => $d_start,
				"end_date" => $d_end,
				"city" => $e->event_town,
				"image_data" => $e->event_img_data, //$this->events_model->get_event_image_resized($e->event_id, 75, 75)->getBase64(),
				"title" => $e->ec_title,
				"url" => $base_url . "info/event/" . $e->event_id,
			);

			if($d_start < $d_now && $d_end < $d_now){
				$past_events[] = $event_post;
			}else{
				$current_events[] = $event_post;				
			}
		}

		$events_list = array(
			"current_events_list" => $current_events,
			"past_events_list" => $past_events
		);

		echo json_encode($events_list);
	}


	
}

?>