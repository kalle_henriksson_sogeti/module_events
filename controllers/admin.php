<?PHP
	error_reporting(E_ALL);
	ini_set('display_errors', '1');

	if (!defined('BASEPATH')) exit('No direct script access allowed');


	
	
/**
@Module:		Modules
@Name:			admin.php
--------------------------------------------------------------------------------------------------
@Creator:		Luckylane, Kalle Henriksson
@Created:		2014
@Version:		1.0
@PHP Version: 	5	
--------------------------------------------------------------------------------------------------
@Description	Denna fil hanterar modulerna som webbplatsen byggs upp med.

@History
DATE			AUTHOR				ACTION
2014-10-14		Kalle Henriksson		Skapade modulen.
	
*/
class Admin extends Admin_Controller 
{				  
	
	function __construct()
	{
		
		parent::Admin_Controller();
		
		// Hämtar in nödvändiga filer
		
		// Läser in libraries
		$this->load->library(array(
			'form_validation',
		));
			
		// Läser in models
		$this->load->model(array(
			'language_model',
			'events/events_model',
			'contact/contact_model',
			'accounts/accounts_model',
		));
		
		// Läser in helpers
		$this->load->helper(array(
			'language',
			'url',
			'events',
			'contact/contact',
			'form',
			'accounts/accounts',
		));
		
	}
	
	/**
	@Name:			function index()
	------------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Param			none
	
	@Return								
	------------------------------------------------------------------------------------------------------------------
	@Description: 	Hämtar in alla registrerade events och skriver ut dessa i en lista.
	
	@History
	DATE			AUTHOR				ACTION
	2015-02-24		Kalle Henriksson	Skapade funktionen.
		
	*/
	function index()
	{
		
		// Hämtar in registrerade moduler.
		$data['events']	=	$this->events_model->get_all_events();
		
		// Skriver ut vyn där modulerna listas upp i en tabell.
		$this->template->build('eventslist', $data);	
	}







	
	/**
	@Name:			add_event()
	---------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Param
	
	@return					
	---------------------------------------------------------------------------------------------------------------
	@Description: 	Visar ett formulär för att registrera ett nytt event i systemet.
	
	@History
	DATE			AUTHOR				ACTION
	2015-02-24		Kalle Henriksson	Skapade funktionen
		
	*/
	function add_event()
	{
		$data['languages']			= $this->language_model->get_available_languages();

		$data['accounts']			= $this->accounts_model->get_accounts();
		// Skriver ut vyn med ett formulär för att registrera ett nytt event i systemet			

		//	Hämtar utställare (företag) möjliga att välja för eventet
		$data['exhibitors'] = $this->events_model->get_accounts_companies();

		$data['selected_exhibitors'] = array();

		$this->template->build('edit_event', $data);
		
	}






	/**
	@Name:			edit_event()
	---------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Param
	
	@return					
	---------------------------------------------------------------------------------------------------------------
	@Description: 	Visar ett formulär för att redigera ett event.
	
	@History
	DATE			AUTHOR				ACTION
	2015-02-24		Kalle Henriksson	Skapade funktionen
		
	*/
	function edit_event(){
		// Sätter ID för aktuellt event
		$event_id					=	$this->uri->segment(4);

		$data['languages']			= $this->language_model->get_available_languages();
		
		$data['accounts']			= $this->accounts_model->get_accounts();
		// Hämtar event från DB
		$data['event'] 				= $this->events_model->get_event($event_id);

		//	Hämtar utställare (företag) möjliga att välja för eventet
		$data['exhibitors'] = $this->events_model->get_accounts_companies();

		//	Hämtar alla utställare (företag) som kopplats till ett event
		$data['selected_exhibitors'] = $this->events_model->get_events_companies_exhibitors($event_id);

		foreach($data['languages'] as $lang)
		{
			$data['event_content'][$lang->language_code] = $this->db->where('ec_language', $lang->language_code)
															->where('ec_event_id', $event_id)
															->get('events_content')
															->row_array();
		}

/*		var_dump($data['event_content']);
		exit();
*/

		// Skriver ut vyn med ett formulär för att redigera ett event			
		$this->template->build('edit_event', $data);
	}











	/**
	@Name:			save_event()
	---------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Param
	
	@return					
	---------------------------------------------------------------------------------------------------------------
	@Description: 	Hanterar både uppdatering av befintligt samt skapande av nytt event och returnerar en events-vy.
	
	@History
	DATE			AUTHOR				ACTION
	2015-02-24		Kalle Henriksson	Skapade funktionen
		
	*/
	function save_event(){

		// Läser in aktiva språk för att kunna registrera/uppdatera posten på samtliga aktiva språk
		$langs = $this->language_model->get_available_languages();
		$event_id = $this->input->post('event_id');

////	SLOPA BILDKONVERTERING - SPARA ISTÄLLET URL TILL BILDEN
//		//	Om bild angivits...
//		if($this->input->post('event_img') != ""){
//			//	Läs in filen baserad på sökväg och Base64-konvertera den.
//			$event_img = base64_encode(file_get_contents($this->input->post('event_img', TRUE)));
//			//	Hitta och ta bort alla whitespace (newline också).
//			$event_img = preg_replace('/\s*/', '', trim($event_img));
//		}else{
//			$event_img = "";
//		}

		// TABELLEN EVENTS
		// Sätter variabler för posten
		$data = array(
			'event_account_id' 				=> $this->input->post('event_account_id'),
			'event_type_of_registration' 	=> $this->input->post('event_type_of_registration'),
			'event_start_date' 				=> $this->input->post('event_start_date'),
			'event_end_date' 				=> $this->input->post('event_end_date'),
			'event_latitude' 				=> $this->input->post('event_latitude'),
			'event_longitude' 				=> $this->input->post('event_longitude'),
			'event_town' 					=> $this->input->post('event_town'),
			'event_img' 					=> $this->input->post('event_img'),
			'event_published' 				=> $this->input->post('event_published'),
			'event_updated' 				=> date("Y-m-d H:i:s"),
			'event_updated_by' 				=> $this->session->userdata('user_id')
		);
		
		if($event_id != ""){
			// Uppdaterar posten		
			$this->db->where('event_id', $event_id)->update('events', $data);
		}else{
			$data["event_created"] = date("Y-m-d H:i:s");
			$data["event_created_by"] = $this->session->userdata('user_id');
			$data["event_status"] = 1;
			// Sparar ny post		
			$this->db->insert('events', $data);
			
			// Sätter id för den post vi just skapade
			$event_id = $this->db->insert_id();
		}

		//	Tar fram bilden för eventet -> Sätter ny storlek -> Encodar till Base64 -> Sparar i event_img_data-fältet i DB.
		$event_image 			= $this->events_model->get_event_image_resized($event_id, 75, 75);
		$img_data 				= array(
									"event_img_data" => $event_image != null ? $event_image->getBase64() : '',
								);
		$this->db->where('event_id', $event_id)->update('events', $img_data);


		
		// TABELLEN EVENTS_CONTENT
		// Uppdaterar postens innehåll (i tabellen events_content) för varje språk...
		foreach($langs as $lang)
		{
			$lang_code = $lang->language_code;

			// Sätter variablar
			$ec_id					=	$this->input->post('ec_id_' . $lang_code);
			$ec_title				=	$this->input->post('ec_title_' . $lang_code);
			$ec_description			=	$this->input->post('ec_description_' . $lang_code);
			$ec_seo_keywords		=	$this->input->post('ec_seo_keywords_' . $lang_code);
			$ec_seo_description		=	$this->input->post('ec_seo_description_' . $lang_code);
			$ec_language			=	$lang->language_code;
						

			$ec = array(
			   'ec_event_id' 			=> $event_id,
			   'ec_title' 				=> $ec_title,
			   'ec_description' 		=> $ec_description,
			   'ec_seo_keywords' 		=> $ec_seo_keywords,
			   'ec_seo_description' 	=> $ec_seo_description,
			   'ec_language'			=> $ec_language
			);				

			// Har vi ett ec_id skall vi UPPDATERA posten
			if($ec_id != "") {								
				$this->db->where('ec_id', $ec_id);				
				$this->db->update('events_content', $ec); 
								
			} else {
				$this->db->insert('events_content', $ec);
			}
		}
				
		// Feedback
		$this->session->set_flashdata('msg', '<div class="message success"><p>Eventet är sparat.</p></div>');
		
		// Visar vyn igen
		redirect('admin/events', 'refresh');
	}



	


	/**
	@Name:			function events_action()
	------------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Return			object					
	------------------------------------------------------------------------------------------------------------------
	@Description: 	Administrerar ett event. Används uteslutande av systemadministratören.
	
	@History
	DATE			AUTHOR				ACTION
	2015-02-24		Kalle Henriksson	Skapade funktionen.
		
	*/
	
	function events_action() {

		$do 	= $this->input->post('do');
		$item 	= $this->input->post('item');
		
		// Skall vi ta bort poster. Observera att vi inte raderar posterna ännu.
		// Vi sätter _status till 0 för att kunna ångra oss.
		if(isset($do['delete']))
		{			
			foreach($item as $id)
			{
				$this->db->where('event_id', $id)->update('events', array('event_status' => '0'));
			}
			
			// Feedback.
			$this->session->set_flashdata('msg', '<div class="message success">Markerade events inaktiverades.</div>');
		
		} elseif(isset($do['change_published'])) {

			// Skall vi byta status (läst/oläst) på posterna gör vi det nu.
			foreach($item as $id)
			{
				$this->db->where('event_id', $id)->update('events', array('event_published' => $this->input->post('event_published')));
			}
			
			// Feedback.
			$this->session->set_flashdata('message', '<div class="message success">Publiceringsstatus bytt</div>');		
		}
		
		// Läser in vyn igen.
		redirect($_SERVER['HTTP_REFERER'], 'refresh');
	}		
}

?>