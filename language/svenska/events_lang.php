<?PHP  

	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	
	/**
	@Module:		Events
	@Name:			events_lang.php
	@Language:		Svenska
	--------------------------------------------------------------------------------------------------
	@Creator:		Sogeti, Kalle Henriksson
	@Created:		2014
	@Version:		1.0
	@PHP Version: 	5	
	--------------------------------------------------------------------------------------------------
	@Description	Denna fil hanterar språkvariablar för modulen.
	
	@History
	DATUM			VEM					ÅTGÄRD
	2015-02-23		Kalle Henriksson	Skapade funktionen.
	
	*/
	
	$lang['events_module_name']						=	"events";
	$lang['events_headline']						=	"Events";
	$lang['events_add_event']						=	"Lägg till event";
	$lang['events_edit_event']						=	"Redigera event";
	$lang['events_title']							=	"Eventnamn";
	$lang['events_start_date']						=	"Startdatum";
	$lang['events_end_date']						=	"Slutdatum";
	$lang['events_start_time']						=	"Starttid";
	$lang['events_end_time']						=	"Sluttid";
	$lang['events_type_of_registration']			=	"Typ av anmälan";
	$lang['events_event_image']						=	"Eventbild";
	$lang['events_choose_pic']						=	"Välj bild";
	$lang['events_delete_pic']						=	"Radera bild";
	$lang['event_was_saved']						=	"Eventet har sparats";
	$lang['events_type_of_registration_event']		=	"Anmälan görs till eventet.";
	$lang['events_type_of_registration_activity']	=	"Anmälan görs till eventets aktiviteter.";
	$lang['events_add_event']						=	"Lägg till event";
	$lang['events_edit_event']						=	"Redigera event";
	$lang['events_map']								=	"Karta";
	$lang['events_seo_keywords']					=	"SEO -nyckelord";
	$lang['events_seo_description']					=	"SEO -beskrivning";
	$lang['events_show_events']						=	"Visa events";
	$lang['events_show_programs']					=	"Visa eventprogram";
	$lang['events_show_activities']					=	"Visa aktivitetsbibliotek";
	$lang['events_exhibitors']						=	"Utställare";
	$lang['events_available_exhibitors']			=	"Tillgängliga utställare";
	$lang['events_selected_exhibitors']				=	"Valda utställare";
	$lang['events_town']							=	"Ort";
	$lang['events_search_map']						=	"Sök plats";
	$lang['events_lecturers']						=	"Föreläsare";
	$lang['events_add_new_lecturer']				=	"Lägg till föreläsare";
	$lang['events_add_new_company']					=	"Lägg till företag";
?>