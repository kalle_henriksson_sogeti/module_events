<?PHP  

	if (!defined('BASEPATH')) exit('No direct script access allowed'); 
	
	/**
	@Module:		Events
	@Name:			add_event.php
	--------------------------------------------------------------------------------------------------
	@Creator:		Sogeti, Kalle Henriksson
	@Created:		2015
	@Version:		1.0
	@PHP Version: 	5	
	--------------------------------------------------------------------------------------------------
	@Description	Hanterar registrering av ett event.
					
	@History
	DATUM			VEM						ÅTGÄRD
	2015-02-24		Kalle Henriksson		Skapade filen	
	
	*/
	/*
	$event = json_decode($event, true);
*/

?>
<h1><?PHP echo lang('events_headline'); ?></h1>
<?PHP  


	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	// Feedback
	echo $this->session->flashdata('message');
	
	echo validation_errors(); 

	$has_event = isset($event) && count($event) == 1;

	$event_id						=	($has_event ? $event[0]->event_id : "");
	$event_account_id				=	($has_event ? $event[0]->event_account_id : "");
	$event_type_of_registration		=	($has_event ? $event[0]->event_type_of_registration : "");
	$event_start_date				=	($has_event ? $event[0]->event_start_date : "");
	$event_end_date					=	($has_event ? $event[0]->event_end_date : "");
	$event_latitude					=	($has_event ? $event[0]->event_latitude : "");
	$event_longitude				=	($has_event ? $event[0]->event_longitude : "");
	$event_town						=	($has_event ? $event[0]->event_town : "");
	$event_img						=	($has_event ? $event[0]->event_img : "");
	$event_published				=	($has_event ? $event[0]->event_published : "");
	$event_created					=	($has_event ? $event[0]->event_created : "");
	$event_created_by				=	($has_event ? $event[0]->event_created_by : "");
	$event_updated					=	($has_event ? $event[0]->event_updated : "");
	$event_updated_by				=	($has_event ? $event[0]->event_updated_by : "");
	/*$event_status					=	($has_event ? $event[0]->event_status : "");*/
/*	$ec_id							=	($has_event ? $event[0]->ec_id : "");
	$ec_title						=	($has_event ? $event[0]->ec_title : "");
	$ec_description					=	($has_event ? $event[0]->ec_description : "");
	$ec_seo_keywords				=	($has_event ? $event[0]->ec_seo_keywords : "");
	$ec_seo_description				=	($has_event ? $event[0]->ec_seo_description : "");*/
	
?>






	<div style="display: inherit;" class="tab">
	    <ul class="tab_nav tab_nav_right">
	        <li class="tab_active"><a id="tablink_1" href="#tab_1" class="tab_active">Grundinformation</a></li>
	        <li class=""><a id="tablink_2" href="#tab_2" class="tab_active">Utställare</a></li>
	    </ul>
	    <div class="tab_content">
	        <div id="tab_1" class="tab_pane tab_pane_full_width tab_active"> <!-- START TAB 1 CONTENT -->










				<div class="content_box">

					<div class="box_header">
						<?php if ($has_event) { ?>
							<h4><?PHP echo lang('events_edit_event'); ?></h4>
						<?php } else { ?>
					    	<h4><?PHP echo lang('events_add_event'); ?></h4>
						<?php } ?>

				        <ul class="lang_select">
				            <?PHP 
				            	if(count($languages) > 1) {
				                    foreach($languages as $lang) { ?>
				                    	<li><a href="#<?php echo $lang->language_code; ?>"><?PHP echo $lang->language_name; ?></a></li>
				            <?PHP 	} 
				                } ?>
				        </ul>		
				    </div>
				    




				    <div class="box_content">
				    	<?php 
							
							// Startar formuläret
				//    		if($has_event){
								echo form_open('admin/events/save_event', array('class'=>'form label-top'));
				//    		}else{
				//				echo form_open('admin/events/create_event', array('class'=>'form label-top'));
				//    		}
							
						?>
							<input type="hidden" name="event_id" value="<?php echo $event_id; ?>" />
							<!-- <input type="hidden" name="event_status" value="<?php /*echo $event_status;*/ ?>" /> -->

				<?PHP 
							
								// Vi skriver ut de fält som skall finnas på respektive språk
								// Dessa återfinns i tabellen news_posts_content				
								foreach($languages as $lang)
								{ 
									$lang_code = $lang->language_code;
									$ec_id = (isset($event_content[$lang_code]['ec_id']) ? $event_content[$lang_code]['ec_id'] : '');
									$ec_title = (isset($event_content[$lang_code]['ec_title']) ? $event_content[$lang_code]['ec_title'] : '');
									$ec_description = (isset($event_content[$lang_code]['ec_description']) ? $event_content[$lang_code]['ec_description'] : '');
									$ec_seo_keywords = (isset($event_content[$lang_code]['ec_seo_keywords']) ? $event_content[$lang_code]['ec_seo_keywords'] : '');
									$ec_seo_description = (isset($event_content[$lang_code]['ec_seo_description']) ? $event_content[$lang_code]['ec_seo_description'] : '');
				 ?>
					    			<fieldset class="lang_tab" id="<?PHP echo $lang->language_code; ?>">

										<input type="hidden" name="ec_id_<?php echo $lang_code; ?>" value="<?PHP echo $ec_id; ?>">

							            <!-- TITEL -->
							    		<div class="field">
							    			<label class="required" for="ec_title"><?PHP echo lang('system_title'); ?></label>
							    			<input type="text" class="xlarge" maxlength="50" name="ec_title_<?php echo $lang_code; ?>" id="ec_title_<?php echo $lang_code; ?>" value="<?PHP echo $ec_title; ?>" />
							    		</div>
							            
							            <!-- BESKRIVNING -->
							    		<div class="field">
							    			<label class="required" for="ec_description"><?PHP echo lang('system_description'); ?></label>
							    			<textarea  class="wysiwyg ec_description xlarge" rel="tiny" cols="50" rows="7" name="ec_description_<?php echo $lang_code; ?>" id="ec_description_<?php echo $lang_code; ?>"><?PHP echo $ec_description; ?></textarea>
							    		</div>

							            <!-- SEO-NYCKELORD -->
							    		<div class="field">
							    			<label class="" for="ec_seo_keywords"><?PHP echo lang('events_seo_keywords'); ?></label>
							    			<input type="text" class="xlarge" maxlength="50" name="ec_seo_keywords_<?php echo $lang_code; ?>" id="ec_seo_keywords_<?php echo $lang_code; ?>" value="<?PHP echo $ec_seo_keywords; ?>" />
							    		</div>
							                        
							            <!-- SEO-BESKRIVNING -->
							    		<div class="field">
							    			<label class="" for="ec_seo_description"><?PHP echo lang('events_seo_description'); ?></label>
							    			<textarea  class="xlarge" rel="tiny" cols="50" rows="7" name="ec_seo_description_<?php echo $lang_code; ?>" id="ec_seo_description_<?php echo $lang_code; ?>"><?PHP echo $ec_seo_description; ?></textarea>
							    		</div>
					            	</fieldset>
				<?PHP
								} 
				?>




				            <!-- ORT -->
				    		<div class="field">
				    			<label class="required" for="event_town"><?PHP echo lang('events_town'); ?></label>
				    			<input type="text" class="xlarge" maxlength="255" name="event_town" id="event_town" value="<?PHP echo $event_town; ?>" />
				    		</div>

				    		<div class="field">
					            <!-- STARTDATUM -->
					    		<div class="floater" style="margin-right: 15px;">
					    			<label class="required" for="event_start_date"><?PHP echo lang('events_start_date'); ?></label>
					    			<input type="text" class="small text jdpicker" maxlength="20" name="event_start_date" id="event_start_date" value="<?php if($this->session->flashdata('event_start_date')) { echo $this->session->flashdata('event_start_date');} else { echo $event_start_date; } ?>" />	    			
					    			<!--input type="date" class="" maxlength="20" name="event_start_date" id="event_start_date" value="<?php if($this->session->flashdata('event_start_date')) { echo $this->session->flashdata('event_start_date');} else { echo $event_start_date; } ?>" /-->
					    		</div>
					            
					            <!-- SLUTDATUM -->
					    		<div class="floater">
					    			<label class="required" for="event_end_date"><?PHP echo lang('events_end_date'); ?></label>
					    			<input type="text" class="small text jdpicker" maxlength="20" name="event_end_date" id="event_end_date" value="<?php if($this->session->flashdata('event_end_date')) { echo $this->session->flashdata('event_end_date');} else { echo $event_end_date; } ?>"  />
					    		</div>
				    		</div>

				            <!-- KOORDINATER -->
				    		<div class="field">
				        	    <!-- LATITUD -->
				    			<div class="floater" style="margin-right: 25px;">
					    			<label for="event_latitude"><?PHP echo lang('system_latitude'); ?></label>
					    			<input type="text" class="small text" maxlength="10" name="event_latitude" id="event_latitude" value="<?php if($this->session->flashdata('event_latitude')) { echo $this->session->flashdata('event_latitude');} else { echo $event_latitude; } ?>" />
				    			</div>
						        <!-- LONGITUD -->
				    			<div class="floater">
					    			<label for="event_longitude"><?PHP echo lang('system_longitude'); ?></label>
					    			<input type="text" class="small text" maxlength="10" name="event_longitude" id="event_longitude" value="<?php if($this->session->flashdata('event_longitude')) { echo $this->session->flashdata('event_longitude');} else { echo $event_longitude; } ?>" />
				    			</div>
				    		</div>




				            <div class="field">
				                <label class="required"><?PHP echo lang('events_map'); ?></label>
				                <div class="">
				            		<?php if($event_latitude != "" && $event_longitude != ""){
				            			$previewVisible = true;
				            		} else {
				            			$previewVisible = false;
				            		} ?>
				        			<a href="#" id="mapPreviewLink" onclick="return false;" class="map-preview" <?php echo $previewVisible ? '' : ' style="display:none;"'; ?>>
				        				<img id="mapPreview" src="https://maps.googleapis.com/maps/api/staticmap?center=<?php echo $event_latitude . ',' . $event_longitude; ?>&zoom=8&size=150x150">            			
				        			</a>
				        		
				            		<a href="#" id="iconMapMarker" onclick="return false;" class="icon-map-marker" <?php echo $previewVisible ? ' style="display:none;"' : ''; ?>><i class="fa fa-picture-o"></i></a>	            		
				                </div>
				            </div>
				            






				    		<!-- ANMÄLNINGSTYP -->
				            <div class="field">
				            	<label class="required" for="event_type_of_registration"><?php echo lang('events_type_of_registration'); ?></label>
				            	<select id="event_type_of_registration" name="event_type_of_registration">
				            		<option value="0" <?php echo ($event_type_of_registration == 0) ? 'selected="selected"' : ''; ?>><?php echo lang('events_type_of_registration_event'); ?></option>
				            		<option value="1" <?php echo ($event_type_of_registration == 1) ? 'selected="selected"' : ''; ?>><?php echo lang('events_type_of_registration_activity'); ?></option>
				            	</select>
				            </div>


				            <!-- EVENTBILD -->
				            <div class="field">
				                <label><?PHP echo lang('events_event_image'); ?>:</label>
				                <input type="text" class="xlarge" name="event_img" id="event_img" value="<?php echo $event_img; ?>" />
				                <a href="javascript:;" onclick="mcImageManager.browse({oninsert : eventsCustomInsert});" class="btn" id="btn_add_img"><?PHP echo lang('events_choose_pic'); ?></a> | <a href="javascript:;" class="btn" onclick="eventsDeleteFile();"> <?PHP echo lang('events_delete_pic'); ?></a>
								
				                <!-- PREVIEW -->
				                <div class="preview">
				                    <img id="preview_img" src="<?PHP echo set_var_value('events_slide_img'); ?>" width="200" height="142" />
				                </div> 
				            </div>


				            <!-- EVENTBILD -->
				            <div class="field">
						    	<label><?PHP echo lang('system_choose') . ' ' . lang('system_account'); ?>:</label>
						        <select name="event_account_id">
						    	<?PHP
									// Skriver ut konton
									// Det konto som posten tillhör sätts som förval i listan
							    	  foreach ($accounts as $post) { ?>
							    		<option value="<?PHP echo $post->account_id; ?>" <?PHP echo ($event_account_id == $post->account_id) ? 'selected="selected"' : ''; ?>>
										<?PHP echo $post->account_title; ?>
					                    </option>
							    <?php } ?>
						    	</select>
				            </div>


				            <!-- PUBLICERAD -->
				    		<div class="field">
				    			<label class="required" for="event_published"><?PHP echo lang('system_published'); ?></label>
								<select id="event_published" name="event_published">
								    <option value="1" <?php echo ($event_published == 1) ? 'selected="selected"' : ''; ?>><?php echo lang('system_published') ?></option>
								    <option value="0" <?php echo ($event_published == 0) ? 'selected="selected"' : ''; ?>><?php echo lang('system_draft') ?></option>
								</select>
				    		</div>

				            <!-- LÄGG TILL -->
				       		<div class="buttonrow">
				       			<input type="submit" name="save" id="save" value="<?PHP echo lang('system_save'); ?>" class="btn" />
				    		</div>
				            
				    	</form>

					    <div id="mapDialog">
							<div id="mapCanvas" style="width:100%; height:100%;">
								
							</div>
					    </div>
				    </div>	<!-- .box_content -->



				</div> <!-- .content_box -->













		    </div> <!-- END TAB 1 CONTENT  -->

		    <div id="tab_2" class="tab_pane tab_pane_full_width"><!-- START TAB 2 CONTENT  -->





				<div class="content_box">

					<div class="box_header">
					    <h4><?PHP echo lang('events_exhibitors'); ?></h4>	
				    </div>
				    


<style type="text/css">
	.one-row{
		display: table;
	}
	.one-row label,
	.one-row select,
	.one-row input,
	{
		padding: 3px;
		display: table-cell;
	}	
	.one-row label
	{
		white-space: nowrap;
	}
</style>

				    <div class="box_content">
				    	<div class="form">

				    		<!-- VISA ALLA VALBARA FÖRELÄSARE UR KONTAKTBIBLIOTEKET -->
				            <div class="field one-row">
				            	<label class=""><?php echo lang('events_available_exhibitors'); ?></label>
				            	<select id="eventExhibitorsSelectList" name="item[]">
									<option value="-1"></option>
								<?PHP

						    	  foreach ($exhibitors as $post) { ?>
						    		<option value="<?PHP echo $post->eca_id; ?>">
									<?PHP echo $post->company_name . ", " . $post->company_town; ?>
				                    </option>
						    	<?php } ?>
				            	</select>
				            	<input type="button" id="add_exhibitor_to_event" value="Lägg till" />
				            </div>

				            <!-- LÄGG TILL -->
<!-- 				       		<div class="buttonrow">
				    		</div>
 -->


		
				    		<!-- VISA ALLA REDAN KOPPLADE FÖRELÄSARE -->

							<table id="tblExhibitors" cellpadding="0" cellspacing="0" width="100%" class="data_table_static">
				            	<thead>
				                    <tr>
						                <th><?PHP echo lang('contact_company_name'); ?></th>
						                <th><?PHP echo lang('contact_company_town'); ?></th>
						                <th><?PHP echo lang('contact_company_email'); ?></th>
						                <th><?PHP echo lang('contact_company_website'); ?></th>
				                        <th class="no-sort">&nbsp;</th>
				                    </tr>
				                </thead>
								<?PHP 
				                
				                    foreach($selected_exhibitors as $post)
				                    { 
				                    
				                ?>
				                    <tr id="page_<?PHP echo $post->ece_id; ?>">
										<td><?PHP echo $post->company_name; ?></td>
										<td><?PHP echo $post->company_town; ?></td>
										<td><a href="mail:<?PHP echo $post->company_email; ?>"><?PHP echo $post->company_email; ?></a></td>
										<td><a href="<?PHP echo $post->company_website; ?>"><?PHP echo $post->company_website; ?></a></td>
				                        <td><a href="#" onclick="return tblExhibitorsOnDeleteItem(<?php echo $post->ece_id; ?>);"><?php echo lang('system_delete'); ?></a></td>
				                    </tr>
				                <?PHP 
				                
				                    } 
				                    
				                ?>
							</table>
						</div>

					</div> <!-- SLUT .box_content-->
				</div> <!-- SLUT .content_box -->		    	
		    </div><!-- END TAB 2 CONTENT  -->









<script type="text/javascript">
	var map = null;
	var marker = null;
	var startLat = <?php echo ($event_latitude != "" && $event_longitude != "") ? $event_latitude : 0; ?>;
	var startLng = <?php echo ($event_latitude != "" && $event_longitude != "") ? $event_longitude : 0; ?>;
	var startZoom = (startLat == 0 && startLng == 0) ? 3 : 14;


	$(document).ready(function () {
	    var mapDialog = $("#mapDialog").dialog({
	        modal: true,
	        width: 800,
	        height: 600,
	        autoOpen: false,
	        title: "Position",
	        resizeStop: function (event, ui) { google.maps.event.trigger(map, 'resize') },
	        open: function (event, ui) { google.maps.event.trigger(map, 'resize'); },
	        dialogClass: "no-close",
			buttons: [{
			  text: "OK",
			  click: function() {
			  	if(marker != null){
			  		//if(console)console.log(marker.getPosition().lat() + ", " + marker.getPosition().lng());
			  		var lat = marker.getPosition().lat();
			  		var lng = marker.getPosition().lng();
			  		$('#event_latitude').val(lat);
			  		$('#event_longitude').val(lng);
			  		$('#mapPreview').attr('src', 'https://maps.googleapis.com/maps/api/staticmap?center='+lat+','+lng+'&zoom=8&size=150x150');
			  		$('#mapPreviewLink').show();
			  		$('#iconMapMarker').hide();
			  	}
			    $(this).dialog( "close" );
			  }
			}],
			close: function( event, ui ) {
			  	if(marker != null)marker.setMap(null);
			  	marker = null;
			}

	    });


    	<?php if($event_latitude == "" || $event_longitude == ""){ ?>
		if (navigator.geolocation) {
	        navigator.geolocation.getCurrentPosition(function(position){
	        	startLat = position.coords.latitude;
	        	startLng = position.coords.longitude;
	        	startZoom = 14;
	        });
	    }
		<?php } ?>


	    $(".icon-map-marker, .map-preview").on("click", function () {
			/*59.37665149631936, 13.508146232604986*/
    	    googleMap("mapCanvas", startLat, startLng, startZoom);

	        $('#mapDialog').dialog('open');
	        return false;
	    });
	});


	function googleMap(selector, lat, lng, zoom) {
	    var myLatlng = new google.maps.LatLng(lat, lng);
	    if (!map) {
			var mapOptions = {
				zoom: zoom,
				center: myLatlng,
  				mapTypeId: google.maps.MapTypeId.TERRAIN
			};
	        map = new google.maps.Map(document.getElementById(selector), mapOptions);
	    } else {
	        map.setCenter(myLatlng);
	    }

	    marker = new google.maps.Marker({
		    position: myLatlng,
		    map: map,
		    title:"Vald position",
		    draggable:true,
		    animation: google.maps.Animation.DROP,
		});

	    google.maps.event.addListener(map, 'click', function(e){
	    	if(marker != null){
	    		marker.setPosition(e.latLng);
	    	}
	    });
	}
</script>