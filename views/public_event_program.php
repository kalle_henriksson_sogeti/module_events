<?PHP  

	if (!defined('BASEPATH')) exit('No direct script access allowed');
	 
	/**
	@Module:		Events
	@Name:			public_event_program.php
	---------------------------------------------------------------------------------------------------------------
	@Creator:		Sogeti, Kalle Henriksson
	@Created:		2015
	@Version:		1.0
	@PHP Version: 	5	
	---------------------------------------------------------------------------------------------------------------
	@Description	Vy för att visa ett program.
	
	
	@History
	DATE			AUTHOR				ACTION
	2015-02-23		Kalle Henriksson	Skapade filen.
		
	*/


	$ep 			= $event_program;
	$ep_activities 	= $program_activities;



?>



<div class="grid_16 event-header">
	<h1>
		<?php echo $p->epc_title; ?>
	</h1>
</div>

<div class="grid_16 event-program-activity-items">
	<?php foreach ($ep_activities as $epa) { ?>
		<div class="event-program-activity-item">
			<h3><?php echo $epa->title; ?></h3>
			<div><?php echo $epa->description; ?></div>
		</div>
	<?php } ?>
</div>


























