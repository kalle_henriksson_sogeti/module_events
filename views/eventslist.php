<?PHP  
    error_reporting(E_ALL);
    ini_set('display_errors', '1');


	if (!defined('BASEPATH')) exit('No direct script access allowed');
	 
	/**
	@Module:		Events
	@Name:			eventslist.php
	---------------------------------------------------------------------------------------------------------------
	@Creator:		Sogeti, Kalle Henriksson
	@Created:		2015
	@Version:		1.0
	@PHP Version: 	5	
	---------------------------------------------------------------------------------------------------------------
	@Description	Denna fil listar upp registrerade events i en lista.
	
	@History
	DATE			AUTHOR				ACTION
	2015-02-24		Kalle Henriksson	Skapade vyn.
		
	*/

?>
<div class="block">

    <div class="block_head">	
    	<h1><?PHP echo lang('events_headline'); ?></h1>
        <h3><?PHP echo anchor('admin/events/add_event', '+ ' . lang('events_add_event')); ?></h3>
        <br />
    </div> <!-- .block_head ends -->
    
    <div class="block_content">
    	<?PHP 
			
			// Feedback
			echo $this->session->flashdata('msg'); 
			
			// Startar formuläret
			echo form_open('admin/events/events_action'); 
			
		?>
    		<table cellpadding="0" cellspacing="0" width="100%" class="data_table">
            	<thead>
                    <tr>
                        <th width="10" class="no-sort"><input type="checkbox" class="check_all" title="Check all" /></th>
                        <th><?PHP echo lang('system_published'); ?></th>
                        <th><?PHP echo lang('system_status'); ?></th>
                        <th><?PHP echo lang('system_title'); ?></th>
                        <th><?PHP echo lang('system_created'); ?></th>
                        <th><?PHP echo lang('system_created_by'); ?></th>
                        <th><?PHP echo lang('system_updated'); ?></th>
                        <th><?PHP echo lang('system_updated_by'); ?></th>
                        <th class="no-sort">&nbsp;</th>
                    </tr>
                </thead>
				<?PHP 
                
                    foreach($events as $post)
                    { 
                    
                ?>
                    <tr id="page_<?PHP echo $post->event_id; ?>">
                        <td><input type="checkbox" name="item[]" value="<?PHP echo $post->event_id; ?>" /></td>
                        <td><?PHP echo get_status($post->event_published); ?></td>
                        <td><?PHP echo get_deleted_status($post->event_status); ?></td>
                        <td><?PHP echo $post->ec_title; ?></td>
                        <td><?PHP echo _system_fix_date($post->event_created,'YYYY-MM-DD'); ?></td>
                        <td><?PHP echo _system_return_username($post->event_created_by,$format=3); ?></td>
                        <td><?PHP echo _system_fix_date($post->event_updated,'YYYY-MM-DD'); ?></td>
                        <td><?PHP echo _system_return_username($post->event_updated_by,$format=3); ?></td>
                        <td><?PHP echo anchor("admin/events/edit_event/" . $post->event_id, lang('system_edit')); ?></td>
                    </tr>
                <?PHP 
                
                    } 
                    
                ?>
			</table>
            
        <p>
			<?PHP echo lang('system_do_with'); ?>: <input type="submit" name="do[delete]" value="<?PHP echo lang('system_delete'); ?>" id="delete"/> | <?PHP echo lang('system_change_status'); ?> 
            <select name="event_published">
                <option value="1"><?PHP echo lang('system_published'); ?></option>
                <option value="0"><?PHP echo lang('system_draft'); ?></option>
            </select> 
            <input type="submit" id="change_published" name="do[change_published]" value="<?PHP echo lang('system_change'); ?>" />
        </p>
		<?PHP
		
			// Stänger formuläret
			echo form_close();
		
		?>
    	
    </div> <!-- .block_content ends -->
    
</div> <!-- .block ends -->