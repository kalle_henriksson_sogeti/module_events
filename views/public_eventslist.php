<?PHP  

	if (!defined('BASEPATH')) exit('No direct script access allowed');
	 
	/**
	@Module:		Modules
	@Name:			public_eventslists.php
	---------------------------------------------------------------------------------------------------------------
	@Creator:		Sogeti, Kalle Henriksson
	@Created:		2015
	@Version:		1.0
	@PHP Version: 	5	
	---------------------------------------------------------------------------------------------------------------
	@Description	Denna vy listar upp aktuella poster i en sorteringsbar tabell.
	
	
	@History
	DATE			AUTHOR				ACTION
	2015-02-23		Kalle Henriksson	Skapade filen.
		
	*/



	function getDateParts($d){
		$a = explode("-", $d);
		$b = array(intval($a[0]), intval($a[1]), intval($a[2]));
		return $b;
	}

	function getMonthName($m){
		$months = array("januari","februari","mars","april","maj","juni","juli","augusti","september","oktober","november","december");
		$i = intval($m);
		return $i > 0 ? $months[$i-1] : "";
	}

	function getDateWithMonth($d){
		$parts = getDateParts($d);
		$m = getMonthName($parts[1]);
		return $parts[2] . " " . $m;
	}

	function isSameMonth($startD, $endD){
		$startX = explode("-", $startD);
		$endX = explode("-", $endD);

		return intval($startX[1]) == intval($endX[1]);
	}


	function getDateText($startDate, $endDate){
		$startParts = getDateParts($startDate);
		$endParts = getDateParts($endDate);

		//	Om samma månad, skriv ut månaden en gång på slutet bara. Ex. 12-13 februari
	    if(isSameMonth($startDate, $endDate)){
	    	//	Men! Om samma start- och slutdatum, skriv ut datum en gång. Ex. 12 februari
	    	if($startParts[2] == $endParts[2]){
		        return $startParts[2] . " " . getMonthName($endParts[1]);
	    	}else{
		        return $startParts[2] . " - " . $endParts[2] . " " . getMonthName($endParts[1]);
	    	}
	    }else{
	    	//	Om olika månader, skriv ut båda månaderna. Ex. 31 juli - 1 augusti
	        return $startParts[2] . " " . getMonthName($startParts[1]) . " - " . $endParts[2] . " " . getMonthName($endParts[1]);
	    }
	}

?>
<!-- RUBRIK/TITEL -->
<div class="grid_16">
	<h1><i class="fa fa-rocket fa-fw"></i> <?PHP //echo lang('sms_categories'); ?>Event</h1>
</div>
<!-- // RUBRIK/TITEL -->

<!-- TELEFONLISTA -->
<div class="grid_9 info_posts event-posts-container">
    


<?php
	function capText($str, $text_limit = 200){
		if (strlen($str) > $text_limit){
			$str = substr($str, 0, strrpos(substr($str, 0, $text_limit), ' ')) . '...';
		}
		return $str;
	}
?>


<?PHP
	$current_url = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'];
	$parsedUrl = parse_url($current_url);
	$base_url = $parsedUrl['scheme'] . '://' . $parsedUrl['host'] . '/';

	if(is_array($events_list))
	{
		
		if(count($events_list) > 0)
		{
			
			echo '<ul class="event-list">';
			
			foreach($events_list as $event)
			{
				$linkStart = '<a href="' . $base_url . 'events/event/' . $event->event_id . '">';
				$linkEnd = '</a>';

				echo '<li class="event-list-item">';
					echo '<h2>';
						echo $linkStart . $event->ec_title . $linkEnd;
					echo '</h2>';
					echo '<h3>';
					echo getDateText($event->event_start_date, $event->event_end_date);
					echo '</h3>';

					if($event->event_img != ""){
						echo '<div class="event-list-item-image">';
							echo $linkStart . '<img src="'.$event->event_img . '" />' . $linkEnd;
						echo '</div>';
					}

					echo '<div class="event-list-item-description">' . capText($event->ec_description) . '</div>';
				echo '</li>';
				
			}
			
			echo '</ul>';
			
		} else {
			
			// Det finns inte något att skriva ut.
			echo '<p>Det finns inga listor för tillfället.</p>';	
			
		}
			
	} else {
		
		// Det finns inte något att skriva ut.
		echo '<p>Det finns inga listor för tillfället.</p>';	
		
	}
	

?>

</div>
<!-- // TELEFONLISTA -->
