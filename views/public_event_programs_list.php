<?PHP  

	if (!defined('BASEPATH')) exit('No direct script access allowed');
	 
	/**
	@Module:		Events
	@Name:			public_event_programs_list.php
	---------------------------------------------------------------------------------------------------------------
	@Creator:		Sogeti, Kalle Henriksson
	@Created:		2015
	@Version:		1.0
	@PHP Version: 	5	
	---------------------------------------------------------------------------------------------------------------
	@Description	Denna vy visar ett events alla program.
	
	
	@History
	DATE			AUTHOR				ACTION
	2015-02-23		Kalle Henriksson	Skapade filen.
		
	*/

	$e = $event[0];

	function getDateParts($d){
		return explode("-", $d);
	}

	function getMonthName($m){
		$months = array("Januari","Februari","Mars","April","Maj","Juni","Juli","Augusti","September","Oktober","November","December");
		$i = intval($m);
		return $months[$i-1];
	}

	function getDateWithMonth($d){
		$parts = getDateParts($d);
		$m = getMonthName($parts[1]);
		return $parts[2] . " " . $m;
	}

	function isSameMonth($startD, $endD){
		$startX = explode("-", $startD);
		$endX = explode("-", $endD);
		return intval($startX) == intval($endX);
	}


	function getDateText($startDate, $endDate){
		$startParts = getDateParts($startDate);
		$endParts = getDateParts($endDate);

	    if(isSameMonth($startDate, $endDate)){
	        return $startParts[2] . " - " . $endParts[2] . " " . getMonthName($endParts[1]);
	    }else{
	        return $startParts[2] . " " . getMonthName($startParts[1]) . " - " . $endParts[2] . " " . getMonthName($endParts[1]);
	    }
	}

	
?>


<div class="grid_16 program-event-header">
	<h1 class="program-event-title">
		<?php echo $e->ec_title; ?>
	</h1>
	<h2 class="program-event-title-dates">
		<?php echo getDateText($e->event_start_date, $e->event_end_date) . ", " . $e->event_town; ?>
	</h2>
</div>

<div class="grid_16 event-programs">
	<?php foreach ($event_programs as $p) { ?>
		<div class="event-program-item">
			<a class="event-program-item-text" href="<?PHP echo BASE_URL; ?>events/program/<?php echo $p->ep_id; ?>"><?php echo $p->epc_title; ?></a><i class="fa fa-chevron-right"></i>
		</div>
	<?php } ?>
</div>


























