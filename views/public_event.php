<?PHP  

	if (!defined('BASEPATH')) exit('No direct script access allowed');
	 
	/**
	@Module:		Events
	@Name:			public_event.php
	---------------------------------------------------------------------------------------------------------------
	@Creator:		Sogeti, Kalle Henriksson
	@Created:		2015
	@Version:		1.0
	@PHP Version: 	5	
	---------------------------------------------------------------------------------------------------------------
	@Description	Denna vy visar ett events startsida.
	
	
	@History
	DATE			AUTHOR				ACTION
	2015-02-23		Kalle Henriksson	Skapade filen.
		
	*/

	$e = $event[0];

	function getDateParts($d){
		return explode("-", $d);
	}

	function getMonthName($m){
		$months = array("Januari","Februari","Mars","April","Maj","Juni","Juli","Augusti","September","Oktober","November","December");
		$i = intval($m);
		return $months[$i-1];
	}

	function getDateWithMonth($d){
		$parts = getDateParts($d);
		$m = getMonthName($parts[1]);
		return $parts[2] . " " . $m;
	}

	function isSameMonth($startD, $endD){
		$startX = explode("-", $startD);
		$endX = explode("-", $endD);
		return intval($startX) == intval($endX);
	}


	function getDateText($startDate, $endDate){
		$startParts = getDateParts($startDate);
		$endParts = getDateParts($endDate);

	    if(isSameMonth($startDate, $endDate)){
	        return $startParts[2] . " - " . $endParts[2] . " " . getMonthName($endParts[1]);
	    }else{
	        return $startParts[2] . " " . getMonthName($startParts[1]) . " - " . $endParts[2] . " " . getMonthName($endParts[1]);
	    }
	}


?>

<script type="text/javascript">
	$(function(){
		$('.event-menu-item a').on('click', function(){

		});
	});
</script>



<?php
	$hasLongText = (strlen($e->ec_description) > 200 ? true : false);
?>

<script type="text/javascript">
		$(function(){
			$('#eventDescriptionShowMore a').on('click', function(){
				$('#eventDescription').toggleClass('event-description-collapsed');
				$('#eventDescriptionShowMore > a > i').toggleClass("fa-chevron-down").toggleClass("fa-chevron-up");
				$('#eventDescriptionShowMore').toggleClass('event-description-show-more-expanded');
				return false;
			});
		});
</script>


<div class="event-image-block">
	<img class="event-image" src="<?php echo $e->event_img; ?>" />
	<div class="event-title-block-compact">
		<h1 class="event-title">
			<?php echo $e->ec_title; ?>
		</h1>
		<h2 class="event-title-dates">
			<?php echo getDateText($e->event_start_date, $e->event_end_date) . ", " . $e->event_town; ?>
		</h2>
	</div>
</div>

<div id="eventDescription" class="event-description-block <?php echo ($hasLongText ? 'event-description-collapsed' : ''); ?>">
	<div class="event-title-block">
		<h1 class="event-title">
			<?php echo $e->ec_title; ?>
		</h1>
		<h2 class="event-title-dates">
			<?php echo getDateText($e->event_start_date, $e->event_end_date) . ", " . $e->event_town; ?>
		</h2>
	</div>
	<?php echo $e->ec_description; ?>
</div>

<div id="eventDescriptionShowMore" class="event-description-show-more"><a href="#"><i class="fa fa-chevron-down"></i></a></div>

<div class="event-links-block event-menu">
	<div class="event-menu-item">
		<a class="event-menu-item-text" href="<?PHP echo BASE_URL; ?>events/programs/<?php echo $e->event_id; ?>">Program</a><i class="fa fa-chevron-right"></i>
	</div>
	<div class="event-menu-item">
		<a class="event-menu-item-text" href="<?PHP echo BASE_URL; ?>events/my_program/<?php echo $e->event_id; ?>">Mitt program</a><i class="fa fa-chevron-right"></i>
	</div>
	<div class="event-menu-item">
		<a class="event-menu-item-text" href="#">Och så vidare</a><i class="fa fa-chevron-right"></i>
	</div>
	<div class="event-menu-item">
		<a class="event-menu-item-text" href="#">Medmera</a><i class="fa fa-chevron-right"></i>
	</div>
	<div class="event-menu-item">
		<a class="event-menu-item-text" href="#">Ännu en</a><i class="fa fa-chevron-right"></i>
	</div>
</div>


























