<?PHP  

	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	
/**
@Module:		Events
@Name:			avents_model.php
--------------------------------------------------------------------------------------------------
@Creator:		Sogeti, Kalle Henriksson
@Created:		2015
@Version:		1.0
@PHP Version: 	5	
--------------------------------------------------------------------------------------------------
@Description	Denna fil hanterar eventmodulen.
				
@History
DATE			AUTHOR				ACTION
2015-02-23		Kalle Henriksson		Skapade modulen.
	
*/
class Events_model extends MY_Model 
{
	
	function __construct()
    {
       
	    parent::Model();
		
    }




    
	
	/**
	@Name:			get_all_events()
	---------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Param
	
	@return			result_array()					
	---------------------------------------------------------------------------------------------------------------
	@Description:	Hämtar alla event för ett visst språk. 	
	
	@History
	DATE			AUTHOR				ACTION
	2015-02-23		Kalle Henriksson		Skapade funktionen
		
	*/
    function get_all_events($language='sv')
	{	
		$query = _events_get_events_data($language)->result();
		
		return $query;
	}	





    
	
	/**
	@Name:			get_events()
	---------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Param			$language, $published, $status, $account_id
					
	@return			result_array()					
	---------------------------------------------------------------------------------------------------------------
	@Description:	Hämtar alla event som inte är borttagna, för ett visst språk. 	
	
	@History
	DATE			AUTHOR				ACTION
	2015-02-23		Kalle Henriksson		Skapade funktionen
	*/
    function get_events($language='sv', $account_id = null)
	{	
		$query = _events_get_events_data($language, 1, null, $account_id)->result();
		
		return $query;
	}





 




    
	
	/**
	@Name:			get_event()
	---------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Param			$event_id, $language
		
	@return			result_array()					
	---------------------------------------------------------------------------------------------------------------
	@Description:	Hämtar ett event för ett visst språk baserat på event_id. 	
	
	@History
	DATE			AUTHOR				ACTION
	2015-02-23		Kalle Henriksson		Skapade funktionen
		
	*/
	function get_event($event_id, $language='sv'){
		$query = _events_get_event_data($event_id, $language)->result();
		
		return $query;
	}	





	/**
	@Name:			get_accounts_companies()
	---------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Param 			$account_id
	
	@return			result_array()					
	---------------------------------------------------------------------------------------------------------------
	@Description:	Hämtar alla företag som finns kopplade till kontot. 	
	
	@History
	DATE			AUTHOR				ACTION
	2012-03-20		Kalle Henriksson	Skapade funktionen
		
	*/
    function get_accounts_companies($account_id=null)
	{

		$query = _events_get_events_companies_accounts_data($account_id)->result();
		
		return $query;	
	}







	/**
	@Name:			get_event_companies()
	---------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Param
	
	@return			result_array()					
	---------------------------------------------------------------------------------------------------------------
	@Description:	Hämtar alla utställare (företag) kopplade till ett event. 	
	
	@History
	DATE			AUTHOR				ACTION
	2012-03-20		Kalle Henriksson	Skapade funktionen
		
	*/
    function get_events_companies_exhibitors($event_id)
	{

		$query = _events_get_events_companies_exhibitors_data($event_id)->result();

		
//		$query	=	$this->db->where('company_status',1)->get('contacts_companies')->result_array();
		        
		return $query;
	
	}





    
	
	/**
	@Name:			get_event_image()
	---------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Param			$event_id
		
	@return			String					
	---------------------------------------------------------------------------------------------------------------
	@Description:	Hämtar bilden för ett visst event. Returnerar Base64-kodad bilddata som en sträng. 	
	
	@History
	DATE			AUTHOR				ACTION
	2015-02-23		Kalle Henriksson		Skapade funktionen
		
	*/
	function get_event_image($event_id){
		$data = _events_get_event_image_data($event_id);
		
		return $data;
	}





	function get_event_image_resized($event_id, $width=0, $height=0){
		$image = _events_get_image_resized_data($event_id, $width, $height);

		return $image;
	}
} 